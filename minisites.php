<?php
/*
Plugin Name: Animafac Minisites
Plugin URI: https://framagit.org/Animafac/wp-minisites
Description: Custom post type used to integrate former mini-websites on animafac.net
Version: 0.1.0
Author: Animafac
Author URI: https://www.animafac.net/
Text Domain: minisites
Domain Path: /languages
 */


require_once __DIR__.'/vendor/autoload.php';

load_plugin_textdomain('minisites', false, 'minisites/languages/');

add_action('init', ['Minisites\\Minisite', 'init']);

add_filter('post_link', ['Minisites\\Minisite', 'permalink'], 10, 3);
add_filter('post_type_link', ['Minisites\\Minisite', 'permalink'], 10, 3);
